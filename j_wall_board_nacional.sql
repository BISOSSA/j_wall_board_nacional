BEGIN
  dbms_scheduler.create_job(job_name        => 'j_wall_board_nacional'
                           ,job_type        => 'STORED_PROCEDURE'
                           ,job_action      => 'dba_sosdw.p_wall_board_nacional'
                           ,start_date      => '04/03/2020 10:00:00 -03:00'
                           ,repeat_interval => 'FREQ=SECONDLY;INTERVAL=30'
                           ,end_date        => NULL
                           ,auto_drop       => FALSE
                           ,enabled         => TRUE);
END;
/
